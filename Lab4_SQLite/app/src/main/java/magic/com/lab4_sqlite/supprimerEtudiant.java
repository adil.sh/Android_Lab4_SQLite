package magic.com.lab4_sqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class supprimerEtudiant extends AppCompatActivity {

    EditText et_supprimer;
    Button bt_supprimer;

    MyDBAdapter mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supprimer);

        et_supprimer = findViewById(R.id.et_idRDV);
        bt_supprimer = findViewById(R.id.bt_supprimer);

        mydb = new MyDBAdapter(this);

        supprimerData();
    }

    public void supprimerData()
    {
        bt_supprimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean isDeleted = mydb.deleteData(et_supprimer.getText().toString());

                if(isDeleted == true)
                {
                    Toast.makeText(supprimerEtudiant.this, "Data Deleted!", Toast.LENGTH_LONG).show();
                }
                else if (isDeleted == false) {
                    Toast.makeText(supprimerEtudiant.this, "Error, data not Deleted!", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}

package magic.com.lab4_sqlite;

public class etudiant {
    private int _id;
    private String _nom;
    private String _prenom;
    private String _programme;
    private String _couriel;
    private String _cours;
    private String _nbh;

    public etudiant()
    {

    }

    public etudiant (String nom, String prenom, String program, String couriel, String cours, String nbh)
    {
        this._nom = nom;
        this._prenom = prenom;
        this._programme = program;
        this._couriel = couriel;
        this._cours = cours;
        this._nbh = nbh;
    }

    // setters and getters

    public void set_id (int _id)
    {
        this._id = _id;
    }

    public void set_nom (String _nom)
    {
        this._nom = _nom;
    }

    public void set_prenom (String _prenom)
    {
        this._prenom = _prenom;
    }

    public void set_programme (String _programme)
    {
        this._programme = _programme;
    }

    public void set_couriel (String _couriel)
    {
        this._couriel = _couriel;
    }

    public void set_cours (String _cours)
    {
        this._cours = _cours;
    }

    public void set_nbh (String nbh)
    {
        this._nbh = _nbh;
    }

    //getters

    public int get_id()
    {
        return  _id;
    }

    public String get_nom()
    {
        return _nom;
    }

    public String get_prenom()
    {
        return _prenom;
    }

    public String get_programme()
    {
        return _programme;
    }

    public String get_couriel()
    {
        return _couriel;
    }

    public String get_cours()
    {
        return _cours;
    }

    public String get_nbh()
    {
        return _nbh;
    }

}

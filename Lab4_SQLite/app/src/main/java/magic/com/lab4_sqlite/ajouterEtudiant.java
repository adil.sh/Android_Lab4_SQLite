package magic.com.lab4_sqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ajouterEtudiant extends AppCompatActivity {

    EditText nom, prenom, program, cours, NbHeures;

    MyDBAdapter dbHelper;
    Button bt_ajouter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter_etudiant);
        dbHelper = new MyDBAdapter(ajouterEtudiant.this);
        nom = findViewById(R.id.e_Nom);
        prenom = findViewById(R.id.e_prenom);
        program = findViewById(R.id.e_programme);
        cours = findViewById(R.id.e_cours);
        NbHeures = findViewById(R.id.e_nbh);


        bt_ajouter = findViewById(R.id.bt_ajouter);



        ajouter_etudiant();

       // printDB();

    }

    public void ajouter_etudiant ()
    {
        bt_ajouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isInserted = dbHelper.insertEtudiant(nom.getText().toString(),prenom.getText().toString(),program.getText().toString(),cours.getText().toString(),NbHeures.getText().toString());

                if(isInserted== true)
                {
                    Toast.makeText(ajouterEtudiant.this, "Data inserted", Toast.LENGTH_LONG).show();
                }
                else if (isInserted == false)
                {
                    Toast.makeText(ajouterEtudiant.this, "Error", Toast.LENGTH_LONG).show();
                }
            }
        });

    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.id_afficher) {
            // write logic here
            Intent i = new Intent(ajouterEtudiant.this, AfficherEtudiants.class);
            startActivity(i);
            return true;
        } else if (id == R.id.id_ajouter) {
            Intent i = new Intent(ajouterEtudiant.this, ajouterEtudiant.class);
            startActivity(i);
            return true;
        } else if (id == R.id.id_modifier) {
            Intent i = new Intent(ajouterEtudiant.this, modifier_etudiant.class);
            startActivity(i);
            return true;

        }    else if (id==R.id.id_supprimer)
        {
            Intent i = new Intent(ajouterEtudiant.this, supprimerEtudiant.class);
            startActivity(i);
            return true;
        }else if (id == R.id.id_afficherRDV) {
            return true;
        } else if (id == R.id.id_ajouterRDV) {
            return true;
        }

        return true;
    }


}
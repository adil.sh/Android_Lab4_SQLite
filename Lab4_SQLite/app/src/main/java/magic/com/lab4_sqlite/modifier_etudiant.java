package magic.com.lab4_sqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class modifier_etudiant extends AppCompatActivity {

    EditText et_id, nom, prenom, program, cours, NbHeures;

    MyDBAdapter myDB;
    Button bt_modifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifier_etudiant);

        myDB = new MyDBAdapter(modifier_etudiant.this);
        nom = findViewById(R.id.e_Nom);
        prenom = findViewById(R.id.e_prenom);
        program = findViewById(R.id.e_programme);
        cours = findViewById(R.id.e_cours);
        NbHeures = findViewById(R.id.e_nbh);
        et_id = findViewById(R.id.et_idRDV);

        bt_modifier = findViewById(R.id.bt_modifier);

        modifierData();
    }

    public void modifierData()
    {
        bt_modifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( TextUtils.isEmpty(nom.getText()) ||TextUtils.isEmpty(prenom.getText()) || TextUtils.isEmpty(program.getText())
                        || TextUtils.isEmpty(cours.getText()) || TextUtils.isEmpty(NbHeures.getText()))
                {
                    Toast.makeText(modifier_etudiant.this, "ERROR, Please complete all fields", Toast.LENGTH_LONG).show();

                    nom.setError("First name is required!");
                    prenom.setError("Last name is required!");
                    program.setError("Program name is required!");
                    cours.setError("Course name is required!");
                    NbHeures.setError("# of hours are required!");
                }
                else {
                    Boolean isUpdated = myDB.updateData(et_id.getText().toString(), nom.getText().toString(), prenom.getText().toString(), program.getText().toString(), cours.getText().toString(), NbHeures.getText().toString());

                    if (isUpdated == true)
                    {
                        Toast.makeText(modifier_etudiant.this, "Data updated!", Toast.LENGTH_LONG).show();
                    } else if (isUpdated == false) {
                        Toast.makeText(modifier_etudiant.this, "Error, data not updated! Please check the ID", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.id_afficher) {
            // write logic here
            Intent i = new Intent(modifier_etudiant.this, AfficherEtudiants.class);
            startActivity(i);
            return true;
        } else if (id == R.id.id_ajouter) {
            Intent i = new Intent(modifier_etudiant.this, ajouterEtudiant.class);
            startActivity(i);
            return true;
        } else if (id == R.id.id_modifier) {
            Intent i = new Intent(modifier_etudiant.this, modifier_etudiant.class);
            startActivity(i);
            return true;

        }    else if (id==R.id.id_supprimer)
        {
            Intent i = new Intent(modifier_etudiant.this, supprimerEtudiant.class);
            startActivity(i);
            return true;
        }else if (id == R.id.id_afficherRDV) {
            return true;
        } else if (id == R.id.id_ajouterRDV) {
            return true;
        }

        return true;
    }
}
